# Dungeon

## How to compile?
```bash
# Go into the project source folder (src)
$ cd dungeon2/src

# Compile the main class
$ javac net/xeill/elpuig/Main.java

# Run the main
$ java net.xeill.elpuig.Main
```
