package net.xeill.elpuig;

import java.util.Random;
import java.util.Scanner;

class IO {

  Scanner input = new Scanner(System.in);
  Colores colores=new Colores();

  public void cls() {
    System.out.print("\033\143");
  }

  public void welcome() {
    System.out.println();
    System.out.println();

    System.out.println();
    System.out.println();

    System.out.println();
    System.out.println();

    System.out.println();

    System.out.println(colores.YELLOW+"Welcome to Puigmania"+)colores.RESET;
    System.out.println(colores.YELLOW+"---Q---U---E---D---A---T---E---E---N---C---A---S---A---!"+colores.RESET);

  }
  public void youLose() {
    System.out.println(colores.RED_BOLD+"You limp out of the puig, weat from battle"+colores.RESET);
  }

  public void youWin() {
    System.out.println(colores.GREEN_BOLD+"You Win"+colores.RESET);
  }


  public int rnd(int min, int max) {
    Random r = new Random();
    return r.nextInt((max - min) + 1) + min;
  }

  public void enemyAppeared(Enemy enemy) {
    System.out.println(colores.RED_BOLD+"\tª" + enemy.name + "  " + "appeared! ª\n"+colores.RESET);
  }

  public void invalidCommand() {
    System.out.println("\t>Invalid Command!");
  }

  public void runAway(Enemy enemy) {
    System.out.println(colores.RED_BOLD+"\t> You run away From the " + enemy.name + " ! "+colores.RESET);
  }

  public void tooWeak() {
    System.out.println(colores.RED_BOLD+"\t> You have recieve too much damage, you are too weak."+colores.RESET);
  }


  public void menuPlayerChoice() {
        System.out.println(colores.GREEN+   " __________________________________ "+colores.RESET);
    System.out.println(colores.BLUE+        "|  \n\tWhat WOuld you Like to do?  |"+colores.RESET);
    System.out.println(colores.BLUE+        "|  \t1. Attack                     |"+colores.RESET);
    System.out.println(colores.BLUE+        "|  \t2. Drink Health Potion        |"+colores.RESET);
    System.out.println(colores.BLUE+        "|  \t3. Run                        |"+colores.RESET);
        System.out.println(colores.GREEN+   " ________________________________"+colores.RESET);
  }

  public int getMenuPlayerChoice() {
      String soption;
      int option;
      try {
        soption = input.nextLine();
        option = Integer.parseInt(soption);
      } catch (Exception e) {
        // Si ha fallado porque ha introducido un valor que no es entero
        // Se borra la pantalla
        cls();
        // Se imprime el menú de inicio
        menuPlayerChoice();
        // Se vuelve a llamar para capturar a sí misma para intentar leer el entero
        return getMenuPlayerChoice();
      }
      return option;
  }

}
